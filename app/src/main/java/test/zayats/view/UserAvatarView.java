package test.zayats.view;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Shader;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.support.annotation.NonNull;
import android.util.AttributeSet;

import java.io.IOException;
import java.io.InputStream;

import test.zayats.R;


public class UserAvatarView extends android.widget.ImageView {

    private int strokeColor;
    private int strokeWidth;
    private String backgroundImage;
    private ShapeDrawable avatar;
    private ShapeDrawable oval;


    public UserAvatarView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomAttributes(context, attrs);
    }

    //доступны методы для определния высоты и ширины
    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        setAvatar();
    }

    private void setCustomAttributes(Context context, AttributeSet attrs) {
        TypedArray customAttributes = null;
        try {
            customAttributes = context.obtainStyledAttributes(attrs, R.styleable.UserAvatarView, 0, 0);
            strokeColor = customAttributes.getColor(R.styleable.UserAvatarView_stroke_color, 0);
            strokeWidth = customAttributes.getDimensionPixelSize(R.styleable.UserAvatarView_stroke_width, 0);
            backgroundImage = customAttributes.getString(R.styleable.UserAvatarView_avatar_asset);
        } finally {
            if (customAttributes != null) {
                customAttributes.recycle();
            }
        }
    }


    //call after view is layouted
    private void setAvatar() {
        InputStream bitmapStream = null;
        try {
            bitmapStream = getResources().getAssets().open(String.format("user/%s", backgroundImage));
            Bitmap userImage = BitmapFactory.decodeStream(bitmapStream);
            userImage = Bitmap.createScaledBitmap(userImage,getWidth()-2*strokeWidth,getHeight()-2*strokeWidth,false);

            BitmapShader shader = new BitmapShader(userImage, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
            avatar = new ShapeDrawable(new OvalShape());
            avatar.getPaint().setShader(shader);
            avatar.getShape().resize(getWidth()-2*strokeWidth,getHeight()-2*strokeWidth);
            avatar.setBounds(strokeWidth,strokeWidth,getWidth()-strokeWidth,getHeight()-strokeWidth);

            oval = new ShapeDrawable(new OvalShape());
            oval.getPaint().setColor(strokeColor);
            oval.getShape().resize(getWidth(),getHeight());

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bitmapStream != null) {
                    bitmapStream.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public void onDraw(@NonNull Canvas canvas) {
        oval.draw(canvas);
        avatar.draw(canvas);
    }
}