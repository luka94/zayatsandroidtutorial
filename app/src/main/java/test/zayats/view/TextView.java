package test.zayats.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

public class TextView extends android.widget.TextView {

    private static String BUTTON_ATTRIBUTE_FONT_NAME = "typeface";
    private static final String SCHEMA = "http://schemas.android.com/custom_view";

    public TextView(Context context) {
        super(context);
    }

    public TextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode())
            setCustomFont(context,attrs);
    }

    public TextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (!isInEditMode())
            setCustomFont(context,attrs);
    }

    /**
     *  XML set font
     *
     * @param @context
     * @param @attrs
     **/

    private void setCustomFont(Context context,AttributeSet attrs){
        String typefaceName = attrs.getAttributeValue(SCHEMA,BUTTON_ATTRIBUTE_FONT_NAME);
        setCustomFont(context,typefaceName);
    }

    /**
     * Use this method to set custom font in code
     *
     * @param context
     * @param customFont
     */

    private void setCustomFont(Context context, String customFont){
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), String.format("fonts/%s", customFont));
        setTypeface(typeface);
    }
}
