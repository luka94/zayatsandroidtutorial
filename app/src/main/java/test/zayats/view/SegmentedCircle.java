package test.zayats.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.ArcShape;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

import test.zayats.R;

public class SegmentedCircle extends SurfaceView implements SurfaceHolder.Callback {

    private float[] mSegmentDegrees;
    private int[] mSegmentColors;
    private int mSegmentCount;
    private float mStart = 0;
    private LayerDrawable mSegmentedCircleDrawable = null;

    public SegmentedCircle(Context context) {
        super(context);
    }

    public SegmentedCircle(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomAttributes(context, attrs);
        getHolder().addCallback(this);
    }

    public SegmentedCircle(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setCustomAttributes(context, attrs);
        getHolder().addCallback(this);
    }

    private void setCustomAttributes(Context context, AttributeSet attrs) {
        TypedArray customAttributes = null;
        TypedArray mSegmentColors = null;
        TypedArray mSegmentPercents = null;

        try {

            customAttributes = context.obtainStyledAttributes(attrs, R.styleable.SegmentedCircle);
            int colorResourceId = customAttributes.getResourceId(R.styleable.SegmentedCircle_segment_colors, 0);
            int percentResourceId = customAttributes.getResourceId(R.styleable.SegmentedCircle_segment_percents, 0);

            if ((colorResourceId != 0) && (percentResourceId != 0)) {
                mSegmentColors = getResources().obtainTypedArray(colorResourceId);
                mSegmentPercents = getResources().obtainTypedArray(percentResourceId);
                setCustomAttributeValues(mSegmentColors, mSegmentPercents);
            }

        } catch (Exceptions.NoMatchColorAndSegmentCountException e) {
            e.printStackTrace();
        } finally {
            if (customAttributes != null) {
                customAttributes.recycle();
            }
        }
    }

    private void setCustomAttributeValues(TypedArray colorsTypedArray, TypedArray percentsTypedArray) throws Exceptions.NoMatchColorAndSegmentCountException {
        if (colorsTypedArray.length() == percentsTypedArray.length()) {
            mSegmentCount = colorsTypedArray.length();
        } else {
            throw new Exceptions.NoMatchColorAndSegmentCountException();
        }

        mSegmentDegrees = new float[mSegmentCount];
        mSegmentColors = new int[mSegmentCount];

        for (int i = 0; i < mSegmentCount; i++) {
            mSegmentColors[i] = colorsTypedArray.getColor(i, Color.BLACK);
            mSegmentDegrees[i] = 360 * percentsTypedArray.getFloat(i, 0);
        }
    }

    private void setDrawableSegments() {
        Drawable[] arcs = new Drawable[mSegmentCount];

        if (mSegmentCount > 0) {
            for (int i = 0; i < mSegmentCount; i++) {
                ShapeDrawable arc = new ShapeDrawable(new ArcShape(mStart, mSegmentDegrees[i]));
                arc.getPaint().setColor(mSegmentColors[i]);
                mStart += mSegmentDegrees[i];
                arcs[i] = arc;
            }
            mSegmentedCircleDrawable = new LayerDrawable(arcs);
        }
    }


    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
       // setDrawableSegments();
    }


    @Override
    public void onDraw(@NonNull Canvas canvas) {
    }


    public void drawOnCanvas(SurfaceHolder holder) {
        final SurfaceHolder mHolder = holder;
        Thread thread = new Thread() {
            @Override
            public void run() {
                Canvas canvas = null;
                RectF rectF = new RectF(0,0,getWidth(),getHeight());
                float start = mStart;
                try {
                    for (int i = 0; i < mSegmentDegrees.length; i++) {
                        Paint paint = new Paint();
                        paint.setColor(mSegmentColors[i]);
                        for (float j = 0; j < mSegmentDegrees[i]; j+=0.3) {
                            canvas = mHolder.lockCanvas();
                            canvas.drawArc(rectF, (float) (start+j), 1, true, paint);
                            mHolder.unlockCanvasAndPost(canvas);
                            Thread.sleep(5);
                        }
                        start+=mSegmentDegrees[i];
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        thread.start();
    }


    public void setStyle(int[] colors, float[] percents, int segmentCount, float start) {
        mSegmentColors = colors;
        mSegmentCount = segmentCount;
        mStart = start;
        for (int i = 0; i < mSegmentCount; i++) {
            mSegmentDegrees[i] = 360 * percents[i];
        }
        //setDrawableSegments();
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        drawOnCanvas(holder);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }

    private static class Exceptions {
        public static class NoMatchColorAndSegmentCountException extends Exception {

        }
    }

}
