package test.zayats;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;


public class NavigationDrawerAdapter extends BaseAdapter implements ListView.OnItemSelectedListener {

    private Activity mActivity;
    private Resources mResources;
    private TypedArray mIcons;
    private String[] mDrawerTitles;
    private TypedArray mVisibility;
    private int mLayout;
    private Typeface typeface;

    private static class ViewHolder {
        TextView titleTextView;
        TextView counterTextView;
    }

    public NavigationDrawerAdapter(Activity activity, Resources resources, int layout, String customFont) {
        super();
        mActivity = activity;
        mResources = resources;
        mIcons = mResources.obtainTypedArray(R.array.navigation_drawer_items);
        mDrawerTitles = mResources.getStringArray(R.array.navigation_drawer_titles);
        mVisibility = mResources.obtainTypedArray(R.array.navigation_drawer_counter);
        mLayout = layout;
        typeface = Typeface.createFromAsset(mActivity.getAssets(), String.format("fonts/%s", customFont));
    }

    @Override
    public int getCount() {
        return mDrawerTitles.length;
    }

    @Override
    public Object getItem(int position) {
        return mDrawerTitles[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = new ViewHolder();

        if (convertView == null) {
            convertView = ((LayoutInflater) (mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE))).inflate(mLayout, null);
            viewHolder.titleTextView = (TextView) convertView.findViewById(R.id.drawer_title);
            TextView counterTextView = (TextView) convertView.findViewById(R.id.drawer_count);
            viewHolder.titleTextView.setText(mDrawerTitles[position]);
            viewHolder.titleTextView.setTypeface(typeface);

//            if ( < Build.VERSION_CODES.JELLY_BEAN) {
                viewHolder.titleTextView.setCompoundDrawables(mIcons.getDrawable(position), null, null, null);
//            } else {
//                viewHolder.titleTextView.setCompoundDrawablesRelativeWithIntrinsicBounds(mIcons.getDrawable(position), null, null, null);
//            }

            counterTextView.setVisibility(mVisibility.getBoolean(position, true) ? View.VISIBLE : View.INVISIBLE);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        return convertView;
    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

}
