package test.zayats;

import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.TreeMap;


public class NavigationDrawerActivity extends ActionBarActivity {

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private TreeMap<Integer, Fragment> fragments;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.navigation_drawer);

        NavigationDrawerAdapter mAdpater = new NavigationDrawerAdapter(this, getResources(), R.layout.drawer_item, "ubuntu_r.ttf");
        mDrawerList = (ListView) (findViewById(R.id.navigation_drawer_list_view));
        mDrawerList.setAdapter(mAdpater);
        mDrawerList.setOnItemSelectedListener(mAdpater);

        mDrawerLayout = (DrawerLayout) (findViewById(R.id.drawer_layout));
        mDrawerLayout.setDrawerListener(new ActionBarDrawerToggle(
                this, mDrawerLayout, R.string.navigation_drawer_open, R.string.navigation_drawer_close));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        initializeFragments();

        getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragments.get(FragmentNames.OS_FRAGMENT)).commit();
    }

    private void initializeFragments() {
        fragments = new TreeMap<>();
        Fragment osFragment = new Fragment() {
            @Override
            public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                                     @Nullable Bundle savedInstanceState) {
                return inflater.inflate(R.layout.os_layout,null);
            }
        };
        fragments.put(FragmentNames.OS_FRAGMENT, osFragment);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.global, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private static class FragmentNames {
        public static final int OS_FRAGMENT = 1;
    }

}
